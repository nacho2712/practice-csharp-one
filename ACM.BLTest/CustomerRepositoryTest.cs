﻿using ACM.BL;
using Xunit;

namespace ACM.BLTest
{
	public class CustomerRepositoryTest
	{
		[Fact]
		public void RetreiveExiting()
		{
			// Organizar
			var customerRepository = new CustomerRepository();
			var expected = new Customer(1)
			{
				Email = "nacho@gmail.com",
				FirstName = "Nacho",
				LastName = "Dominguez"
			};

			// Actuar
			var actual = customerRepository.Retreive(1);

			// Verificar
			// Para probar si un objeto es igual al que se espera,
			// Se tiene que validar propiedad por propiedad para corroborar.
			//Assert.Equal(expected, actual); -> esto no es valido, porque aún así las propiedades tengan el mismo valor, se condirarán diferentes porque no hacen referencia al mismo objeto.
			Assert.Equal(expected.CustomerId, actual.CustomerId);
			Assert.Equal(expected.Email, actual.Email);
			Assert.Equal(expected.FirstName, actual.FirstName);
			Assert.Equal(expected.LastName, actual.LastName);
		}

	}
}
