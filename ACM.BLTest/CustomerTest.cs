using ACM.BL;
using System;
using Xunit;

namespace ACM.BLTest
{
	public class CustomerTest
	{
		[Fact(Skip = "")]
		public void FullNameTestValid()
		{
			//	Organizar -> Preparamos para realizar la prueba que estamos necesitando.
			var customer = new Customer()
			{
				//Name = "Bilbo",
				LastName = "Baggins"
			};

			string expected = "Baggins";

			//	Actuar -> Realizamos la operación que estamos esperando.
			string actual = customer.FullName;

			//	Assert -> Verificar que el valor esperado coincida con el actual.
			Assert.Equal(expected, actual);
		}

		[Fact]
		public void StaticTest()
		{
			// -- Organizar
			var c1 = new Customer();
			c1.FirstName = "Bilbo";
			Customer.InstanceCount += 1;

			var c2 = new Customer();
			c2.FirstName = "Frodo";
			Customer.InstanceCount += 1;

			var c3 = new Customer();
			c3.FirstName = "Rosie";
			Customer.InstanceCount += 1;

			// -- Actuar

			// -- Assert
			Assert.Equal(3, Customer.InstanceCount);
		}

		[Fact]
		public void TestValidate()
		{
			// -- Arrange
			var customer = new Customer();
			customer.LastName = "Baggins";
			customer.Email = "fbaggins@hobbiton.me";

			var expected = true;

			// -- Act 
			var actual = customer.Validate();

			// -- Assert
			Assert.Equal(expected, actual);
		}
	}
}
