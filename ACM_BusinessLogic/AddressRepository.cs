﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACM.BL
{
	public class AddressRepository
	{
		public Address Retreive(int addressId)
		{
			var address = new Address(addressId);

			if (addressId == 1)
			{
				address.StreetLine1 = "Calle 1";
				address.StreetLine2 = "Esquina";
				address.City = "Cordoba";
				address.State = "Cordoba";
				address.Country = "Argentina";
				address.PostalCode = "5006";
			}

			return address;
		}

		public IEnumerable<Address> RetreiveByCustomerId(int customerId)
		{
			var addressList = new List<Address>();
			var address = new Address(1)
			{
				StreetLine1 = "Calle 1",
				StreetLine2 = "Esquina",
				City = "Cordoba",
				State = "Cordoba",
				Country = "Argentina",
				PostalCode = "5006",
			};
			addressList.Add(address);

			address = new Address(2)
			{
				StreetLine1 = "Calle 1",
				StreetLine2 = "Esquina",
				City = "Cordoba",
				State = "Cordoba",
				Country = "Argentina",
				PostalCode = "5006",
			};
			addressList.Add(address);

			return addressList;
		}
	}
}
