﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACM.BL
{
	public class Customer
	{
		// Constructor por defecto
		public Customer()
			: this(0) // -> Encadenamiento de constructores.
		{

		}

		//
		public Customer(int customerId)
		{
			this.CustomerId = customerId;
			this.Address = new List<Address>(); 
		}

		public int CustomerId { get; private set; }

		public static int InstanceCount { get; set; }

		public string FirstName  { get; set; }

		public string LastName { get; set; }

		public string FullName
		{
			get
			{
				if (string.IsNullOrEmpty(this.FirstName) || string.IsNullOrEmpty(this.LastName))
					return string.Concat(this.FirstName, this.LastName);
				return string.Join(", ", LastName, FirstName);
			}
		}

		public string Email { get; set; }

		public List<Address> Address { get; set; } 

		public bool Validate()
		{
			if (string.IsNullOrEmpty(LastName)) return false;
			if (string.IsNullOrEmpty(LastName)) return false;

			return true;
		}
	}
}
