﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACM.BL
{
	public class CustomerRepository
	{
		private AddressRepository addressRepository = new AddressRepository();

		public CustomerRepository()
		{
			addressRepository = new AddressRepository();
		}

		public Customer Retreive(int customerId)
		{
			var customer = new Customer(customerId);
			customer.Address = addressRepository.
					RetreiveByCustomerId(customer.CustomerId).ToList();
			
			if(customer.CustomerId == 1)
			{
				customer.Email = "nacho@gmail.com";
				customer.FirstName = "nacho";
				customer.LastName = "dominguez";
			}

			return customer;
		}

		public List<Customer> Retreive()
		{
			return new List<Customer>();
		}

		public bool Save()
		{
			return false;
		}
	}
}
