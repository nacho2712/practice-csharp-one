﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACM.BL
{
	public class Order
	{
		public Order()
		{
			
		}

		public Order(int orderId)
		{
			this.OrderId = orderId;
		}

		public DateTimeOffset? OrderDate { get; set; }

		public int OrderId { get; private set; }

		public List<Order> orderItems { get; set; }

		//public List<OrderItem> Items { get; set; }

		public bool Validate()
		{
			//if (string.IsNullOrEmpty(ProductName)) return false;
			if (OrderDate == null) return false;

			return true;
		}
	}
}
