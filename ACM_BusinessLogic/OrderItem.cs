﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACM.BL
{
	public class OrderItem
	{
		public OrderItem()
		{

		}

		public OrderItem(int orderItemId)
		{
			this.OrderItemId = orderItemId;
		}

		public int OrderItemId { get; private set; }

		public int OrderQuantity { get; set; }

		public int ProductId { get; set; }

		public decimal? PurchasePrice { get; set; }

		public bool Validate()
		{
			if (OrderQuantity <= 0) return false;
			if (ProductId <= 0) return false;
			if (PurchasePrice == null) return false;

			return true;
		}

		public Order Retreive(int orderId)
		{
			return new Order();
		}

		public bool Save()
		{
			return false;
		}
	}
}
