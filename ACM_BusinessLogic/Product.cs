﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACM.BL
{
	public class Product
	{
		public Product()
		{

		}

		public Product(int productId)
		{
			this.ProductId = productId;
		}

		public Decimal? CurrentPrice { get; set; }

		public int ProductId { get; private set; }

		public string ProductDescription { get; set; }

		public string ProductName { get; set; }
		

		public bool Validate()
		{
			if (string.IsNullOrEmpty(ProductName)) return false;
			if (CurrentPrice == null) return false;

			return true;
		}
	}
}
